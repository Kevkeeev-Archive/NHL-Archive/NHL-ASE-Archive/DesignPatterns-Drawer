package nl.kevinnauta.designpatternsdrawer;

import java.util.ArrayList;

public class CommandStack {
	private DrawerFrame listener;
	
    protected int currentState; 
    private int getCurrentState() { return currentState; }
    private void setCurrentState( int newState ) { 
    	this.currentState = ( newState >= -1 ? (newState < getCommandStackSize() ? newState : getCommandStackSize() ) : -1);
	}

    protected ArrayList<Command> commandStack;
    private int getCommandStackSize() { return commandStack.size(); }
    private void pushCommand(Command newCommand) { commandStack.add( newCommand ); }
    private Command popCommand() { 
    	Command cmd = commandStack.get( commandStack.size()-1 ); 
    	commandStack.remove( commandStack.size()-1 );
    	return cmd;
    }

    private String readDescription() { return commandStack.get( getCurrentState() ).getDescription(); }

    public boolean canUndo() { return getCurrentState() > -1; }
    public boolean canRedo() { return getCurrentState() < getCommandStackSize()-1; }
    
    /**
     * Constructor for a command Stack
     * @param newListener Pointer to the DrawerFrame instance
     */
    public CommandStack(DrawerFrame newListener) {
    	commandStack = new ArrayList<Command>();
    	currentState = 0;
    	this.listener = newListener;
    }
    
    /**
     * Method to go back one step in the commandStack
     */
    public void undoCommand() {
    	commandStack.get( getCurrentState() ).revive();
    	listener.setText("Undo last action: " + readDescription() );
    	setCurrentState( getCurrentState() - 1);
    	listener.onNotify();
    }
    
    /**
     * Method to go forward one step in the commandStack
     */
    public void redoCommand() {
    	setCurrentState( getCurrentState() + 1);
    	commandStack.get( getCurrentState() ).execute();
    	listener.setText("Redo next action: " + readDescription() );
    	listener.onNotify();
    }
    
    /**
     * Method to add a command to the stack, i.e. user does an action.
     * @param newCommand The action performed to add to the stack.
     */
    public void doCommand(Command cmd) {
    	// User has undone some actions, and does a new action at that point. First remove all undone actions.
    	while( getCommandStackSize()-1 > getCurrentState() ) {
    		DEBUG.Print("Popping undone command: " + popCommand().getDescription() );
    	}
    	pushCommand(cmd);
    	setCurrentState( getCommandStackSize() - 1 );
    	cmd.execute();
    	DEBUG.Print(this.toString());
    	listener.setText("Do action: " + readDescription());
    	listener.onNotify();
    }
    
    public String toString(){
    	String tmp = "CMD[]: " + getCommandStackSize() + " cmd's, now at cmd #" + getCurrentState();
    	for(Command cmd : commandStack) {
    		tmp += "\nCMD " + commandStack.indexOf(cmd) + " = " + cmd.getDescription();
    	}
    	return tmp;
    }
}
