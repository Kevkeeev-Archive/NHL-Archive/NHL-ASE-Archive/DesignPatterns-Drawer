package nl.kevinnauta.designpatternsdrawer;

public class CommandAddFigure implements Command {
	@Override
	public String getDescription() {
		return "Add Figure '" + figure.getClass().getSimpleName() + "'" +
		(figure instanceof BaseItem ? " (" + ((BaseItem) figure).getType().toString().toLowerCase() + ")" : "") + " to group " + groupID;
	}

	private DrawerModel model; //pointer
	private DrawerModel snapshot;
	private Figure figure;
	private int groupID;
	
	public CommandAddFigure(DrawerModel model, Figure figure, int groupID) {
		this.snapshot = new DrawerModel(model.getListener());
		this.model = model;
		this.figure = figure;
		this.groupID = groupID;
	}
	
	@Override
	public void execute() {
		snapshot.setFromClone(model);
		model.addNewItem(figure, groupID);
		model.selectID( figure.getID() ); // Automatically select the new Figure.
	}

	@Override
	public void revive() {
		model.setFromClone(snapshot);
	}

}
