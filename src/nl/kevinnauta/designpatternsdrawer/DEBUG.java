package nl.kevinnauta.designpatternsdrawer;

public final class DEBUG {
	public static boolean DEBUG = true;
	public static boolean ID = true;
	public static boolean NOTIFY = false;
	public static boolean PRINTGROUP = true;
	public static boolean PRINTALLGROUPS = false;
	public static void save(boolean value) { ID = value; }


	public static void Trace(Group group){
        Print(group.toString());
        Trace();
	}
	
	public static void Trace(){
    	new Throwable().printStackTrace(System.out);
	}
	
	/**
	 * Use this function to print debug statements when you want, or disable them by setting boolean DEBUG to false.
	 * @param msg
	 */
	public static void Print(String msg) {
		if(DEBUG) { System.out.println("[DBG] " + msg); }
	}
	
	/**
	 * Use this function to disable/enable printing the model status every time DrawerFrame.onNotify() is called.
	 * @param msg
	 */
	public static void Notify(String msg) {
		if(NOTIFY) { Print(msg); }
	}
	
	/**
	 * Use this function to find out where or if a certain location in code is executed.
	 * @param msg The File's capital letters, semicolon, linenumber. Ex: DPD:5
	 */
	public static void Use(String msg) {
		Print("I'm usefull! @" + msg);
	}
}
