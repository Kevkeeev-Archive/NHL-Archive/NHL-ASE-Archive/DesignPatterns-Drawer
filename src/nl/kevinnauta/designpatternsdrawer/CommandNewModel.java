package nl.kevinnauta.designpatternsdrawer;

public class CommandNewModel implements Command {
	@Override
	public String getDescription() { return "Making a new model."; }
	
	private DrawerModel model; //pointer
	private DrawerModel snapshot;
	
	public CommandNewModel(DrawerModel model) {
		this.model = model;
		this.snapshot = new DrawerModel(model.getListener());
	}
	
	@Override
	public void execute() {
		snapshot.setFromClone(model);
		model.removeFigure(0);
	}

	@Override
	public void revive() {
		model.setFromClone( snapshot );
	}

}
