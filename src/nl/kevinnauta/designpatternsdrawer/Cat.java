package nl.kevinnauta.designpatternsdrawer;

import javax.swing.*;

import nl.kevinnauta.designpatternsdrawer.CommandSelectFigure.MODE;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Handles mouse. :D
 */
public class Cat implements MouseListener, MouseMotionListener {
	DrawerFrame listener;
	Point origin;

	public Cat(DrawerFrame listener) {
		this.listener = listener;
	}

	public void mousePressed(MouseEvent e) {
		origin = e.getPoint();
	}

	public void mouseReleased(MouseEvent e) {
		origin = null;
		Figure selectedFigure = null;

		if(SwingUtilities.isLeftMouseButton(e)) {

			if (listener.getModel().drawGroup != null && listener.getModel().drawGroup.width * listener.getModel().drawGroup.height > 25) {
				//was dragging
				listener.menuEditEditItem(listener.getModel().drawGroup, e.getPoint());

			} else { //clicking, or dragging area smaller than 5*5-ish so probably clicked.
				selectedFigure = listener.getModel().getItemAtLocation(e.getX(), e.getY());
				if(selectedFigure != null && selectedFigure.getID() != 0) {
					if( e.isControlDown() ) {
						listener.commandStack.doCommand(new CommandSelectFigure(listener.getModel(), selectedFigure.getID(), MODE.ADD));
					}  else {
						if(! (listener.getModel().getSelectedIDs().size() == 1 && listener.getModel().getSelectedIDs().contains(selectedFigure.getID()) )){
							listener.commandStack.doCommand(new CommandSelectFigure(listener.getModel(), selectedFigure.getID(), MODE.SET));
						} //only add this command if the item to select isn't already selected.
					}
					listener.setText("Found this item: " + selectedFigure.toString());
				} else {
					if( listener.getModel().hasAnySelected() ){
						listener.commandStack.doCommand(new CommandSelectFigure(listener.getModel(), 0, MODE.SET));
						listener.setText("Deselected the item(s).");
					}
				}
			}
		} else if( SwingUtilities.isRightMouseButton(e)) {
			if (! listener.getModel().getSelectedIDs().contains(0)) {
				listener.menuEditMenu(listener.getModel().getSelectedIDs(), e.getX(), e.getY());
			} else {
				listener.menuEditMenu(e.getX(), e.getY());
			}
		}

		listener.getModel().drawGroup = null;
		listener.onNotify();
	}

	public void mouseDragged(MouseEvent e) {
		if( SwingUtilities.isLeftMouseButton(e)) {
			Point cur = e.getPoint();

			int xb = (origin.x < cur.x ? origin.x : cur.x);
			int yb = (origin.y < cur.y ? origin.y : cur.y);
			int xe = Math.abs(origin.x - cur.x);
			int ye = Math.abs(origin.y - cur.y);

			listener.getModel().drawGroup = new Rectangle(xb, yb, xe, ye);
			listener.onNotify();
		}
	}
	
	//Unused
	public void mouseClicked(MouseEvent me) {}
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}
	public void mouseMoved(MouseEvent me) {}

}
