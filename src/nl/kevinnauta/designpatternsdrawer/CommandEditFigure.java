package nl.kevinnauta.designpatternsdrawer;

public class CommandEditFigure implements Command {
	@Override
	public String getDescription() { return "Edit Figure " + oldFigureID + "."; }
	
	private DrawerModel model; //pointer
	private DrawerModel snapshot;
	private int oldFigureID;
	private Figure editedFigure;
	
	public CommandEditFigure(DrawerModel model, int oldFigureID, Figure editedFigure) {
		this.snapshot = new DrawerModel(model.getListener());
		this.model = model;
		this.oldFigureID = oldFigureID;
		this.editedFigure = editedFigure;
	}
	
	@Override
	public void execute() {
		snapshot.setFromClone(model);
		model.placeItemAtLocation(editedFigure, oldFigureID);
	}

	@Override
	public void revive() {
		model.setFromClone(snapshot);
	}

}
